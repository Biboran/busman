from django import forms
from django.utils.translation import gettext_lazy as _
from django.utils import timezone

from .models import Region, BusPark, PublicComplaint


class ScatterChartForm(forms.Form):
    GRANULARITY_CHOICES = (
        (1, _('Год')),
        (2, _('Месяц')),
        (3, _('Неделя')),
        (4, _('День')),
    )
    REGION_CHOICES = [(0, _('Все'))] + [
        (r.id, str(r)) for r in Region.objects.all()
    ]
    from_date = forms.DateField(label=_('Начало периода'),
                                initial=timezone.datetime(2018, 1, 1))
    to_date = forms.DateField(label=_('Конец периода'), initial=timezone.now())
    granularity = forms.ChoiceField(choices=GRANULARITY_CHOICES,
                                    label=_('Период'))
    region = forms.ChoiceField(choices=REGION_CHOICES, label=_('Регион'))
    cumulative = forms.BooleanField(label=_('Итоговый'), required=False)


class BarChartForm(forms.Form):
    REGION_CHOICES = [(0, _('Все'))] + [
        (r.id, str(r)) for r in Region.objects.all()
    ]
    from_date = forms.DateField(label=_('Начало периода'),
                                initial=timezone.datetime(2018, 1, 1))
    to_date = forms.DateField(label=_('Конец периода'), initial=timezone.now())

    region = forms.ChoiceField(choices=REGION_CHOICES, label=_('Регион'))


class PieChartForm(forms.Form):
    from_date = forms.DateField(label=_('Начало периода'),
                                initial=timezone.datetime(2018, 1, 1))
    to_date = forms.DateField(label=_('Конец периода'), initial=timezone.now())


class UploadFileForm(forms.Form):
    file = forms.FileField()
    template = forms.ChoiceField(
        choices=((1, 'template 1(водители)'), (2, 'template 2(автопарки)')))
    create_driver = forms.BooleanField(label='Создать водителя если нету'
                                             '(будут фэйковые поля!)',
                                       required=False)

    def __init__(self, *args, **kwargs):
        super(UploadFileForm, self).__init__(*args, **kwargs)
        self.fields['bus_park'] = forms.ChoiceField(choices=[(0, '-')] + [
            (b.id, str(b)) for b in BusPark.objects.all()
        ])


class PublicComplaintForm(forms.ModelForm):
    class Meta:
        model = PublicComplaint
        labels = {
            'driver': 'ФИО водителя(если известно)',
            'message': 'Ваш отзыв',
            'phone_number': 'Ваш сотовый телефон для обратной свзяи',
            'email': 'Ваша электронная почта (если имеется)',
        }
        fields = ['route', 'driver', 'message', 'email', 'phone_number', ]
