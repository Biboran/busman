# Generated by Django 2.1.3 on 2018-11-22 20:19

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0011_userpasswordhistory_created'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='driver',
            name='certificate_date',
        ),
    ]
