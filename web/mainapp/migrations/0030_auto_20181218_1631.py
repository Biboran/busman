# Generated by Django 2.1.3 on 2018-12-18 10:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0029_kpicache'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='kpicache',
            name='kpi_date',
        ),
        migrations.AddField(
            model_name='kpicache',
            name='kpi_date_ordinal',
            field=models.PositiveIntegerField(default=0),
            preserve_default=False,
        ),
    ]
