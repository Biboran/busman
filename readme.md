Чтобы запустить эту байду на деве, нужно следующее

* git clone
* cd busman
* cp configs/.env.dev.template .env
* docker-compose build
* docker-compose up -d
* создать юзера docker-compose exec web python manage.py createsuperuser
* зайти в админку http://127.0.0.1/admin


backup:
docker exec busman_db_1 pg_dump -U busman_dev busman_dev > busman_backup

restore:
docker exec busman_db_1 createdb -U busman_dev -T template0 busman_backup
docker cp busman_backup busman_db_1:busman_backup
docker exec busman_db_1 psql -U busman_dev busman_backup -f busman_backup
