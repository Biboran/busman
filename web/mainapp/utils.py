import logging
import time
import inspect
from django.db import transaction
import xlrd
import datetime
from mainapp.models import *

logger = logging.getLogger(__name__)


def template_1_loader(file):
    wb = xlrd.open_workbook(file_contents=file.read())
    sh = wb.sheet_by_index(0)
    for i, raw in enumerate(sh.get_rows()):
        if i == 0:
            continue

        date = datetime.datetime(*xlrd.xldate_as_tuple(raw[0].value, wb.datemode))
        protocol_number = str(raw[2].value).strip()
        first_name = str(raw[3].value).strip().lower()
        last_name = str(raw[4].value).strip().lower()
        patronymic = str(raw[5].value).strip().lower()
        try:
            birth_date = datetime.datetime(
                *xlrd.xldate_as_tuple(raw[6].value, wb.datemode))
        except:
            birth_date = None
        iin = str(raw[7].value).strip()
        gov_number = str(raw[9].value).strip()
        model = str(raw[10].value).strip()
        preventive_measure = str(raw[11].value).strip()
        cash_amount = int(raw[12].value)
        recovery_amount = int(raw[13].value)
        penalty_type = '%s - %s' % (
            str(raw[14].value).strip(), str(raw[15].value).strip())
        yield (date, protocol_number, first_name, last_name, patronymic,
               birth_date, iin, gov_number, model, preventive_measure,
               cash_amount,
               recovery_amount, penalty_type)


def template_2_loader(file):
    wb = xlrd.open_workbook(file_contents=file.read())
    sh = wb.sheet_by_index(0)
    for i, raw in enumerate(sh.get_rows()):
        if i == 0:
            continue

        gov_number = str(raw[0].value)

        date = datetime.datetime(
            *xlrd.xldate_as_tuple(raw[4].value, wb.datemode)).date()
        penalty_type = str(raw[6].value)

        yield (gov_number, date, penalty_type)


@transaction.atomic
def template_1(file, create_driver=False):
    for (date, pn, fn, ln, ptr, bd, iin, gn, m,
         pm, ca, ra, pt) in template_1_loader(file):
        driver = Driver.objects.filter(iin=iin[:12]).first()

        if not driver and create_driver:
            driver = Driver.objects.create(
                iin=iin,
                first_name=fn,
                last_name=ln,
                patronymic=ptr,
                birth_date=bd if bd else datetime.date(1990, 1, 1),
                gender=1,
                nationality=Nationality.objects.get(name='Казах'),
                id_card_number=iin,
                id_card_issue_entity='no',
                id_card_issue_date=datetime.date(2000, 1, 1),
                registration_address='no',
                registration_region=Region.objects.get(name='Алматы'),
                work_start_date=datetime.date(2010, 1, 1),
                phone_number='+7'
            )

        if driver:
            penalty_type = PenaltyType.objects.filter(name=pt).first()
            if not penalty_type:
                penalty_type = PenaltyType.objects.create(name=pt, value=-5)
            Penalty.objects.create(
                penalty_type=penalty_type,
                driver=driver,
                date=date,
                protocol_number=pn,
                gov_number=gn,
                auto_model=m,
                preventive_measure=pm,
                cash_amount=ca,
                recovery_amount=ra
            )


@transaction.atomic
def template_2(file, bus_park_id):
    for gn, date, pt in template_2_loader(file):
        penalty_type = PenaltyType.objects.filter(name=pt).first()
        if not penalty_type:
            penalty_type = PenaltyType.objects.create(name=pt, value=-5)

        Penalty.objects.create(
            bus_park_id=bus_park_id,
            penalty_type=penalty_type,
            date=date,
            gov_number=gn
        )


def get_class_that_defined_method(meth):
    if inspect.ismethod(meth):
        for cls in inspect.getmro(meth.__self__.__class__):
            if cls.__dict__.get(meth.__name__) is meth:
                return cls
        meth = meth.__func__  # fallback to __qualname__ parsing
    if inspect.isfunction(meth):
        cls = getattr(inspect.getmodule(meth),
                      meth.__qualname__.split('.<locals>', 1)[0].rsplit('.', 1)[
                          0])
        if isinstance(cls, type):
            return cls
    return None


def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            class_name = get_class_that_defined_method(method)
            logger.debug('%r  %2.2f ms [%r:%r]' % (
                method.__name__, (te - ts) * 1000, method.__module__,
                class_name))
        return result

    return timed
