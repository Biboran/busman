from django import template
from mainapp.models import BusParkUser, Driver

register = template.Library()


@register.filter
def modulo(v, m):
    return v % m


@register.simple_tag
def is_buspark_user(user):
    if str(user) == 'AnonymousUser':
        return False
    bpu = BusParkUser.objects.filter(user=user).first()
    return bpu is not None

@register.filter
def is_buspark_user_filter(user):
    if str(user) == 'AnonymousUser':
        return False
    bpu = BusParkUser.objects.filter(user=user).first()
    return bpu is not None


@register.simple_tag
def driver_photo(path):
    if 'mainapp/driver/' in path and 'change' in path:
        try:
            url = Driver.objects.get(id=path.split('/')[3]).photo.url
            print(url)
            return url
        except:
            pass
    return ''