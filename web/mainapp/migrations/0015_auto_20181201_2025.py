# Generated by Django 2.1.3 on 2018-12-01 14:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0014_auto_20181125_0236'),
    ]

    operations = [
        migrations.AddField(
            model_name='achievementtype',
            name='is_temporal',
            field=models.BooleanField(default=False, verbose_name='Временный'),
        ),
        migrations.AddField(
            model_name='certificatetype',
            name='is_temporal',
            field=models.BooleanField(default=False, verbose_name='Временный'),
        ),
        migrations.AddField(
            model_name='complainttype',
            name='is_temporal',
            field=models.BooleanField(default=True, verbose_name='Временный'),
        ),
        migrations.AddField(
            model_name='penaltytype',
            name='is_temporal',
            field=models.BooleanField(default=True, verbose_name='Временный'),
        ),
    ]
