import datetime

from django.utils.translation import gettext as _
from django.views.generic import View, CreateView
from django.shortcuts import render
from mainapp.models import Driver, Penalty, Achievement, Certificate, \
    Complaint, BusPark, PublicComplaint, JobApplication
from mainapp.forms import ScatterChartForm, BarChartForm, PieChartForm, \
    UploadFileForm, PublicComplaintForm
from mainapp.charts import driver_line_chart, bus_park_line_chart, \
    bus_park_drivers_chart, driver_kpi_chart, driver_points_bar_chart, \
    bus_park_points_bar_chart, bus_park_range_bar_chart, \
    bus_park_regions_pie_chart


class DriverChartsView(View):
    model = Driver
    template_name = 'admin/driver_charts.html'

    def get_form(self, chart_type, data=None):
        if chart_type == 'scatter':
            return ScatterChartForm(data)
        if chart_type == 'bar':
            return BarChartForm(data)
        if chart_type == 'pie':
            return PieChartForm(data)

    def get(self, request, chart_type, pk):
        request.POST._mutable = True
        request.POST.update(
            {
                'from_date': datetime.date(2016, 1, 1),
                'to_date': datetime.date.today(),
                'granularity': 2,
                'region': 0,
            })
        return self.post(request, chart_type, pk)

    def post(self, request, chart_type, pk):
        context = {}
        form = self.get_form(chart_type, data=request.POST)
        context['form'] = form

        if form.is_valid():
            if chart_type == 'scatter':
                f = form.cleaned_data['from_date']
                t = form.cleaned_data['to_date']
                g = int(form.cleaned_data['granularity'])
                c = bool(form.cleaned_data['cumulative'])
                context['charts'] = [
                    driver_line_chart(Penalty, _('Штрафы водителя'), pk, f, t,
                                      g,
                                      c),
                    driver_line_chart(Complaint,
                                      _('Отзывы на водителя'), pk, f, t, g, c),
                    driver_line_chart(Achievement, _('Достижения водителя'),
                                      pk, f, t, g, c),
                    driver_line_chart(Certificate,
                                      _('Сертификаты водителя'), pk, f, t, g,
                                      c),
                    driver_kpi_chart(pk, f, t, g, c)
                ]
            if chart_type == 'bar':
                f = form.cleaned_data['from_date']
                t = form.cleaned_data['to_date']
                context['charts'] = [
                    driver_points_bar_chart(pk, f, t)
                ]
        context['object'] = Driver.objects.get(id=pk)
        context['chart_type'] = chart_type
        return render(request, self.template_name, context)


class BusParkChartsView(View):
    model = Driver
    template_name = 'admin/bus_park_charts.html'

    def get_form(self, chart_type, data=None):
        if chart_type == 'scatter':
            return ScatterChartForm(data)
        if chart_type == 'bar':
            return BarChartForm(data)
        if chart_type == 'pie':
            return PieChartForm(data)

    def get(self, request, chart_type, pk):
        request.POST._mutable = True
        request.POST.update(
            {
                'from_date': datetime.date(2016, 1, 1),
                'to_date': datetime.date.today(),
                'granularity': 2,
                'region': 0,
            })
        return self.post(request, chart_type, pk)

    def post(self, request, chart_type, pk):
        context = {}
        form = self.get_form(chart_type, data=request.POST)
        context['form'] = form

        if form.is_valid():
            if chart_type == 'scatter':
                f = form.cleaned_data['from_date']
                t = form.cleaned_data['to_date']
                g = int(form.cleaned_data['granularity'])
                c = bool(form.cleaned_data['cumulative'])
                r = int(form.cleaned_data['region'])
                context['charts'] = [
                    bus_park_line_chart(Penalty, _('Штрафы'),
                                        pk, f, t, g, c, r),
                    bus_park_line_chart(Complaint,
                                        _('Отзывы'),
                                        pk, f, t, g, c, r),
                    bus_park_line_chart(Achievement,
                                        _('Достижения'),
                                        pk, f, t, g, c, r),
                    bus_park_line_chart(Certificate,
                                        _('Сертификаты'),
                                        pk, f, t, g, c, r),
                    bus_park_drivers_chart(
                        pk, _('Количество водителей'), _('Количество'), 'count',
                        f, t, g, r),
                    # bus_park_drivers_chart(
                    #     pk, _('Средняя зарплата водителей'), _('Зарплата'),
                    #     'salary',
                    #     f, t, g, r),
                    bus_park_drivers_chart(
                        pk, _('Средний возраст водителей'), _('Возраст'), 'age',
                        f, t, g, r),
                    bus_park_drivers_chart(
                        pk, _('Средний опыт водителей'), _('Опыт работы'),
                        'experience',
                        f, t, g, r),
                    bus_park_drivers_chart(
                        pk, _('Количество увольнений'), _('Увольнения'),
                        'dismissal',
                        f, t, g, r),
                    bus_park_drivers_chart(
                        pk, _('Средний KPI'), _('KPI'), 'kpi',
                        f, t, g, r)]
            if chart_type == 'bar':
                f = form.cleaned_data['from_date']
                t = form.cleaned_data['to_date']
                r = int(form.cleaned_data['region'])
                context['charts'] = [
                    bus_park_points_bar_chart(pk, f, t, r),
                    bus_park_range_bar_chart(pk, f, t, r, 'age'),
                    bus_park_range_bar_chart(pk, f, t, r, 'experience'),
                    # bus_park_range_bar_chart(pk, f, t, r, 'salary'),
                    bus_park_range_bar_chart(pk, f, t, r, 'kpi'),

                ]
            if chart_type == 'pie':
                f = form.cleaned_data['from_date']
                t = form.cleaned_data['to_date']
                context['charts'] = [
                    bus_park_regions_pie_chart(pk, f, t)
                ]

        context['object'] = BusPark.objects.get(id=pk)
        context['chart_type'] = chart_type

        return render(request, self.template_name, context)


class AlmatyChartsView(View):
    model = Driver
    template_name = 'admin/almaty_charts.html'

    def get_form(self, chart_type, data=None):
        if chart_type == 'scatter':
            return ScatterChartForm(data)
        if chart_type == 'bar':
            return BarChartForm(data)
        if chart_type == 'pie':
            return PieChartForm(data)

    def get(self, request, chart_type):
        request.POST._mutable = True
        request.POST.update(
            {
                'from_date': datetime.date(2016, 1, 1),
                'to_date': datetime.date.today(),
                'granularity': 2,
                'region': 0,
            })
        return self.post(request, chart_type)

    def post(self, request, chart_type):
        context = {}
        form = self.get_form(chart_type, data=request.POST)
        context['form'] = form
        pk = None

        if form.is_valid():
            if chart_type == 'scatter':
                f = form.cleaned_data['from_date']
                t = form.cleaned_data['to_date']
                g = int(form.cleaned_data['granularity'])
                c = bool(form.cleaned_data['cumulative'])
                r = int(form.cleaned_data['region'])
                context['charts'] = [
                    bus_park_line_chart(Penalty, _('Штрафы'),
                                        pk, f, t, g, c, r),
                    bus_park_line_chart(Complaint,
                                        _('Отзывы'),
                                        pk, f, t, g, c, r),
                    bus_park_line_chart(Achievement,
                                        _('Достижения'),
                                        pk, f, t, g, c, r),
                    bus_park_line_chart(Certificate,
                                        _('Сертификаты'),
                                        pk, f, t, g, c, r),
                    bus_park_drivers_chart(
                        pk, _('Количество водителей'), _('Количество'), 'count',
                        f, t, g, r),
                    # bus_park_drivers_chart(
                    #     pk, _('Средняя зарплата водителей'), _('Зарплата'),
                    #     'salary',
                    #     f, t, g, r),
                    bus_park_drivers_chart(
                        pk, _('Средний возраст водителей'), _('Возраст'), 'age',
                        f, t, g, r),
                    bus_park_drivers_chart(
                        pk, _('Средний опыт водителей'), _('Опыт работы'),
                        'experience',
                        f, t, g, r),
                    bus_park_drivers_chart(
                        pk, _('Количество увольнений'), _('Увольнения'),
                        'dismissal',
                        f, t, g, r),
                    bus_park_drivers_chart(
                        pk, _('Средний KPI'), _('KPI'), 'kpi',
                        f, t, g, r)]
            if chart_type == 'bar':
                f = form.cleaned_data['from_date']
                t = form.cleaned_data['to_date']
                r = int(form.cleaned_data['region'])
                context['charts'] = [
                    bus_park_points_bar_chart(pk, f, t, r),
                    bus_park_range_bar_chart(pk, f, t, r, 'age'),
                    bus_park_range_bar_chart(pk, f, t, r, 'experience'),
                    # bus_park_range_bar_chart(pk, f, t, r, 'salary'),
                    bus_park_range_bar_chart(pk, f, t, r, 'kpi'),

                ]
            if chart_type == 'pie':
                f = form.cleaned_data['from_date']
                t = form.cleaned_data['to_date']
                context['charts'] = [
                    bus_park_regions_pie_chart(pk, f, t)
                ]

        context['chart_type'] = chart_type

        return render(request, self.template_name, context)


class PublicComplaintView(CreateView):
    model = PublicComplaint
    form_class = PublicComplaintForm
    template_name = 'public/complaint.html'
    success_url = '/complaint/success/'


class JobApplicationView(CreateView):
    model = JobApplication
    template_name = 'public/job.html'
    success_url = '/job/success/'
    fields = ['name', 'age',  'work_exp', 'drive_exp', 'category_d_exp',
              'phone_number', 'about']


def upload_file(request):
    from django.contrib import messages
    from .utils import template_1, template_2
    if request.method == 'POST':
        print('is post')
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            f = request.FILES['file']
            t = form.cleaned_data['template']
            create_driver = form.cleaned_data['create_driver']
            bus_park_id = int(form.cleaned_data['bus_park'])

            if int(t) == 1:
                template_1(f, create_driver=create_driver)
            else:
                if bus_park_id == 0:
                    messages.add_message(
                        request, messages.ERROR,
                        'Для template_2 нужно указать автопарк')
                    return render(request, 'upload.html',
                                  {'form': form})

                template_2(f, bus_park_id)

            messages.add_message(
                request, messages.INFO,
                'Загружено.')
            return render(request, 'upload.html', {'form': UploadFileForm()})
    else:
        form = UploadFileForm()
    return render(request, 'upload.html', {'form': form})
