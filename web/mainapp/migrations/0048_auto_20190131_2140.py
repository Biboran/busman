# Generated by Django 2.1.3 on 2019-01-31 15:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0047_auto_20190110_0019'),
    ]

    operations = [
        migrations.CreateModel(
            name='BlackListDriver',
            fields=[
            ],
            options={
                'verbose_name': 'Черный список',
                'verbose_name_plural': 'Черный список',
                'proxy': True,
                'indexes': [],
            },
            bases=('mainapp.driver',),
        ),
        migrations.CreateModel(
            name='MyDriver',
            fields=[
            ],
            options={
                'verbose_name': 'Мой водитель',
                'verbose_name_plural': 'Мои водители',
                'proxy': True,
                'indexes': [],
            },
            bases=('mainapp.driver',),
        ),
        migrations.AlterModelOptions(
            name='publiccomplaint',
            options={'verbose_name': 'Отзыв', 'verbose_name_plural': 'Отзывы'},
        ),
    ]
