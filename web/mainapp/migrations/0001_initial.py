# Generated by Django 2.1.3 on 2018-11-11 21:49

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Achievement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(verbose_name='Дата получения')),
                ('comment', models.TextField(verbose_name='Комментарий')),
                ('achievement_type', models.ForeignKey(on_delete='PROTECT', to='mainapp.Achievement')),
            ],
            options={
                'verbose_name': 'Достижение водителя',
                'verbose_name_plural': 'Достижения водителей',
            },
        ),
        migrations.CreateModel(
            name='AchievementType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(max_length=255, verbose_name='Название')),
                ('value', models.FloatField()),
            ],
            options={
                'verbose_name': 'Тип достижения',
                'verbose_name_plural': 'Типы достижений',
            },
        ),
        migrations.CreateModel(
            name='BusPark',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Название')),
                ('address', models.TextField(verbose_name='Адрес')),
                ('phone_number', models.CharField(max_length=255, verbose_name='Номер телефона')),
            ],
            options={
                'verbose_name': 'Автобусный/троллейбусный парк',
                'verbose_name_plural': 'Автобусные/троллеуйбные парки',
            },
        ),
        migrations.CreateModel(
            name='Certificate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(verbose_name='Дата получения')),
                ('certificate_number', models.CharField(max_length=255)),
                ('comment', models.TextField(verbose_name='Комментарий')),
            ],
            options={
                'verbose_name': 'Сертификат водителя',
                'verbose_name_plural': 'Сертификаты водителей',
            },
        ),
        migrations.CreateModel(
            name='CertificateType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(max_length=255, verbose_name='Название')),
                ('value', models.FloatField()),
            ],
            options={
                'verbose_name': 'Тип сертификата',
                'verbose_name_plural': 'Типы сертификатов',
            },
        ),
        migrations.CreateModel(
            name='Complaint',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(verbose_name='Дата жалобы')),
                ('comment', models.TextField(verbose_name='Комментарий')),
            ],
            options={
                'verbose_name': 'Жалоба на водителя',
                'verbose_name_plural': 'Жалобы на водителей',
            },
        ),
        migrations.CreateModel(
            name='ComplaintType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(max_length=255, verbose_name='Название')),
                ('value', models.FloatField()),
            ],
            options={
                'verbose_name': 'Тип жалобы',
                'verbose_name_plural': 'Типы жалоб',
            },
        ),
        migrations.CreateModel(
            name='Driver',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=255, verbose_name='Имя')),
                ('last_name', models.CharField(max_length=255, verbose_name='Фамилия')),
                ('patronymic', models.CharField(max_length=255, verbose_name='Отчество')),
                ('birth_date', models.DateField(verbose_name='Дата рождения')),
                ('gender', models.PositiveSmallIntegerField(choices=[(1, 'Мужчина'), (2, 'Женщина')], verbose_name='Пол')),
                ('nationality', models.CharField(max_length=255, verbose_name='Национальность')),
                ('id_card_number', models.CharField(max_length=255, verbose_name='Номер удостоверения')),
                ('id_card_issue_entity', models.CharField(max_length=255, verbose_name='Орган выдачи удостоверения')),
                ('id_card_issue_date', models.DateField(verbose_name='Дата выдачи удостоверения')),
                ('certificate_number', models.CharField(max_length=255, verbose_name='Номер сертификата обучения')),
                ('certificate_type', models.CharField(max_length=255, verbose_name='Тип сертификата')),
                ('certificate_date', models.CharField(max_length=255, verbose_name='Дата сертификата')),
                ('registration_address', models.TextField(verbose_name='Адрес прописки')),
                ('registration_region', models.TextField(verbose_name='Регион прописки')),
                ('phone_number', models.CharField(max_length=255, verbose_name='Номер телефона')),
            ],
            options={
                'verbose_name': 'Водитель',
                'verbose_name_plural': 'Водители',
            },
        ),
        migrations.CreateModel(
            name='DriverEmploymentHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('employment_date', models.DateField(verbose_name='Дата трудоустройства')),
                ('dismissal_date', models.DateField(blank=True, null=True, verbose_name='Дата увольнения')),
                ('salary', models.IntegerField(verbose_name='Зарплата')),
                ('dismissal_cause', models.TextField(verbose_name='Причина увольнения')),
                ('remarks', models.TextField(verbose_name='Замечания')),
                ('penalties', models.TextField(verbose_name='Наказания')),
                ('bus_park', models.ForeignKey(on_delete='PROTECT', to='mainapp.BusPark')),
                ('driver', models.ForeignKey(on_delete='PROTECT', to='mainapp.Driver')),
            ],
            options={
                'verbose_name': 'Запись трудоустройства водителя',
                'verbose_name_plural': 'Записи трудоустройств водителей',
            },
        ),
        migrations.CreateModel(
            name='Penalty',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(verbose_name='Дата штрафа')),
                ('comment', models.TextField(verbose_name='Комментарий')),
                ('driver', models.ForeignKey(on_delete='PROTECT', to='mainapp.Driver')),
            ],
            options={
                'verbose_name': 'Штраф водителя',
                'verbose_name_plural': 'Штрафы на водителей',
            },
        ),
        migrations.CreateModel(
            name='PenaltyType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(max_length=255, verbose_name='Название')),
                ('value', models.FloatField()),
            ],
            options={
                'verbose_name': 'Тип штрафа',
                'verbose_name_plural': 'Типы штрафов',
            },
        ),
        migrations.CreateModel(
            name='RoadSpecialist',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=255, verbose_name='Имя')),
                ('last_name', models.CharField(max_length=255, verbose_name='Фамилия')),
                ('patronymic', models.CharField(max_length=255, verbose_name='Отчество')),
                ('start_of_work', models.DateTimeField(verbose_name='Дата начала деятельности')),
                ('iin', models.CharField(max_length=12)),
                ('current_bus_park', models.ForeignKey(null=True, on_delete='SET_NULL', to='mainapp.BusPark')),
            ],
            options={
                'verbose_name': 'Специалист БДД',
                'verbose_name_plural': 'Специалисты БДД',
            },
        ),
        migrations.CreateModel(
            name='RoadSpecialistEmploymentHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('employment_date', models.DateField(verbose_name='Дата трудоустройства')),
                ('dismissal_date', models.DateField(blank=True, null=True, verbose_name='Дата увольнения')),
                ('bus_park', models.ForeignKey(on_delete='PROTECT', to='mainapp.BusPark')),
                ('road_specialist', models.ForeignKey(on_delete='PROTECT', to='mainapp.RoadSpecialist')),
            ],
            options={
                'verbose_name': 'Запись трудоустройства специалиста БДД',
                'verbose_name_plural': 'Записи трудоустройств специалистов БДД',
            },
        ),
        migrations.AddField(
            model_name='penalty',
            name='penalty_type',
            field=models.ForeignKey(on_delete='PROTECT', to='mainapp.PenaltyType'),
        ),
        migrations.AddField(
            model_name='complaint',
            name='complaint_type',
            field=models.ForeignKey(on_delete='PROTECT', to='mainapp.ComplaintType'),
        ),
        migrations.AddField(
            model_name='complaint',
            name='driver',
            field=models.ForeignKey(on_delete='PROTECT', to='mainapp.Driver'),
        ),
        migrations.AddField(
            model_name='certificate',
            name='certificate_type',
            field=models.ForeignKey(on_delete='PROTECT', to='mainapp.CertificateType'),
        ),
        migrations.AddField(
            model_name='certificate',
            name='driver',
            field=models.ForeignKey(on_delete='PROTECT', to='mainapp.Driver'),
        ),
        migrations.AddField(
            model_name='achievement',
            name='driver',
            field=models.ForeignKey(on_delete='PROTECT', to='mainapp.Driver'),
        ),
    ]
