from django.core.management.base import BaseCommand
from django.db import transaction

from mainapp.models import *


class Command(BaseCommand):

    @transaction.atomic
    def handle(self, *args, **options):
        for deh in DriverEmploymentHistory.objects.filter(
            dismissal_date=None
        ):
            deh.driver.current_buspark = deh.bus_park
            deh.driver.save()
