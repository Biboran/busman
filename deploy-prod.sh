#!/usr/bin/env bash
set -e
docker login repo.treescale.com
cd web
docker build . -f docker/Dockerfile -t repo.treescale.com/busman/busman:prod
docker push repo.treescale.com/busman/busman:prod
