# Generated by Django 2.1.3 on 2019-01-09 17:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0042_publiccomplaint_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='publiccomplaint',
            name='email',
            field=models.EmailField(blank=True, max_length=254, null=True, verbose_name='Email'),
        ),
    ]
