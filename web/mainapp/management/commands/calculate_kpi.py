from django.core.management.base import BaseCommand
from mainapp.tasks import calc_kpi, set_current_kpi


class Command(BaseCommand):
    def handle(self, *args, **options):
        calc_kpi()
        set_current_kpi()
