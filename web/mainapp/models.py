from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User, Group
from django.db.models import PROTECT, CASCADE

from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.contrib.auth.models import Permission


BUS_PARK_USER_GROUP_NAME = u'Автобусный Парк'


class Nationality(models.Model):
    name = models.CharField(max_length=255, unique=True)

    class Meta:
        verbose_name = _('Национальность')
        verbose_name_plural = _('Национальности')

    def __str__(self):
        return self.name


class Region(models.Model):
    name = models.CharField(max_length=255, unique=True)

    class Meta:
        verbose_name = _('Регион')
        verbose_name_plural = _('Регионы')

    def __str__(self):
        return self.name


class BusPark(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('Название'))
    address = models.TextField(verbose_name=_('Адрес'))
    phone_number = models.CharField(max_length=255,
                                    verbose_name=_('Номер телефона'))
    current_kpi = models.IntegerField(verbose_name=_('Текущий средний KPI'))

    class Meta:
        verbose_name = _('Автобусный/троллейбусный парк')
        verbose_name_plural = _('Автобусные/троллеуйбусные парки')

    def __str__(self):
        return self.name


class Driver(models.Model):
    GENDER_TYPE = (
        (1, _('Мужчина')),
        (2, _('Женщина'))
    )

    ATTESTAT_STATUS_TYPE = (
        (1, _('Пройдено')),
        (2, _('Не пройдено')),
    )

    photo = models.ImageField(null=True, blank=True, upload_to='photos/',
                              verbose_name=_('Фото'))
    cv = models.FileField(null=True, blank=True, upload_to='cv/',
                          verbose_name=_('Резюме/Анкета'))
    last_name = models.CharField(max_length=255, verbose_name=_('Фамилия'))
    first_name = models.CharField(max_length=255, verbose_name=_('Имя'))
    patronymic = models.CharField(max_length=255, verbose_name=_('Отчество'),
                                  null=True, blank=True)
    birth_date = models.DateField(verbose_name=_('Дата рождения'))
    gender = models.PositiveSmallIntegerField(
        choices=GENDER_TYPE, verbose_name=_('Пол'))
    nationality = models.ForeignKey('Nationality',
                                    verbose_name=_('Национальность'),
                                    on_delete=models.PROTECT)
    id_card_number = models.CharField(
        max_length=255, verbose_name=_('Номер удостоверения'), unique=True,
        null=True, blank=True)
    id_card_issue_entity = models.CharField(
        max_length=255, verbose_name=_('Орган выдачи удостоверения'),
        null=True, blank=True)
    id_card_issue_date = models.DateField(
        verbose_name=_('Дата выдачи удостоверения'),
        null=True, blank=True)
    iin = models.CharField(max_length=12, verbose_name=_('ИИН'), unique=True)
    registration_address = models.TextField(verbose_name=_('Адрес прописки'))
    registration_region = models.ForeignKey('Region',
                                            verbose_name=_('Регион прописки'),
                                            on_delete=models.PROTECT)
    work_start_date = models.DateField(verbose_name=_('Дата начала трудовой деятельности'))
    phone_number = models.CharField(max_length=255, verbose_name=_('Номер телефона'), blank=True, null=True)
    route_number = models.CharField(max_length=15, verbose_name=_('# Маршрута'), null=True, blank=True)

    attestat_status = models.PositiveIntegerField(choices=ATTESTAT_STATUS_TYPE,
                                                  verbose_name=_('Аттестация'),
                                                  null=True, blank=True)
    attestat_points = models.PositiveIntegerField(null=True, blank=True,
                                                  verbose_name=_(
                                                      'Бал аттестации'))
    attestat_date = models.DateField(null=True, blank=True,
                                     verbose_name=_('Дата аттестации'))

    history = models.ManyToManyField('BusPark',
                                     through='DriverEmploymentHistory',
                                     verbose_name=_('История трудоустройств'))
    certificates = models.ManyToManyField('CertificateType',
                                          through='Certificate',
                                          verbose_name=_('Сертификаты'))
    achievements = models.ManyToManyField('AchievementType',
                                          through='Achievement',
                                          verbose_name=_('Достижения'))
    complaints = models.ManyToManyField('ComplaintType',
                                        through='Complaint',
                                        verbose_name=_('Отзывы'))
    penalties = models.ManyToManyField('PenaltyType',
                                       through='Penalty',
                                       verbose_name=_('Штрафы'))
    current_kpi = models.IntegerField(verbose_name=_('Текущий KPI'),
                                      null=True, blank=True)

    current_buspark = models.ForeignKey(BusPark,
                                        verbose_name=_('Текущий автопарк'),
                                        null=True, blank=True,
                                        on_delete=models.SET_NULL,
                                        related_name='current_drivers')

    class Meta:
        verbose_name = _('Водитель')
        verbose_name_plural = _('Водители')

    def __str__(self):
        return '%s %s %s' % (
            self.last_name, self.first_name, self.patronymic)


class DriverEmploymentHistory(models.Model):
    driver = models.ForeignKey('Driver', on_delete=PROTECT,
                               verbose_name=_('Водитель'))
    bus_park = models.ForeignKey('BusPark', on_delete=PROTECT,
                                 verbose_name=_('Автобусный парк'))
    employment_date = models.DateField(
        verbose_name=_('Дата трудоустройства'))
    dismissal_date = models.DateField(null=True, blank=True,
                                      verbose_name=_('Дата увольнения'))
    salary = models.IntegerField(verbose_name=_('Зарплата'), null=True)
    dismissal_cause = models.TextField(verbose_name=_('Причина увольнения'),
                                       null=True, blank=True)
    remarks = models.TextField(verbose_name=_('Замечания'),
                               null=True, blank=True)

    class Meta:
        verbose_name = _('Запись трудоустройства водителя')
        verbose_name_plural = _('Записи трудоустройств водителей')

    def save(self, *args, **kwargs):
        if self.driver:
            if not DriverEmploymentHistory.objects.filter(driver=self.driver, dismissal_date=None).exists():
                self.driver.current_buspark = None
            else:
                self.driver.current_buspark = self.bus_park
            self.driver.save()
        super().save(*args, **kwargs)

    def __str__(self):
        return _('Запись трудоустройства') + ' #%s' % self.id


class RoadSpecialist(models.Model):
    first_name = models.CharField(max_length=255, verbose_name=_('Имя'))
    last_name = models.CharField(max_length=255, verbose_name=_('Фамилия'))
    patronymic = models.CharField(max_length=255, verbose_name=_('Отчество'))
    start_of_work = models.DateField(
        verbose_name=_('Дата начала деятельности'))
    iin = models.CharField(max_length=12, verbose_name=_('ИИН'),
                           unique=True)

    class Meta:
        verbose_name = _('Специалист БДД')
        verbose_name_plural = _('Специалисты БДД')

    def __str__(self):
        return '%s %s %s' % (self.last_name, self.first_name, self.patronymic)


class RoadSpecialistEmploymentHistory(models.Model):
    bus_park = models.ForeignKey('BusPark', on_delete=PROTECT)
    road_specialist = models.ForeignKey('RoadSpecialist', on_delete=PROTECT)
    employment_date = models.DateField(
        verbose_name=_('Дата трудоустройства'))
    dismissal_date = models.DateField(null=True, blank=True,
                                      verbose_name=_('Дата увольнения'))

    class Meta:
        verbose_name = _('Запись трудоустройства специалиста БДД')
        verbose_name_plural = _('Записи трудоустройств специалистов БДД')


class ComplaintType(models.Model):
    name = models.TextField(max_length=255, verbose_name=_('Название'))
    value = models.FloatField()

    is_temporal = models.BooleanField(
        default=True,
        verbose_name=_('Временный')
    )

    class Meta:
        verbose_name = _('Тип отзыва')
        verbose_name_plural = _('Типы отзывов')

    def __str__(self):
        return self.name


class PenaltyType(models.Model):
    name = models.TextField(max_length=255, verbose_name=_('Название'))
    value = models.FloatField()

    is_temporal = models.BooleanField(
        default=True,
        verbose_name=_('Временный')
    )

    class Meta:
        verbose_name = _('Тип штрафа')
        verbose_name_plural = _('Типы штрафов')

    def __str__(self):
        return self.name


class CertificateType(models.Model):
    name = models.TextField(max_length=255, verbose_name=_('Название'))
    value = models.FloatField()
    is_temporal = models.BooleanField(
        default=False,
        verbose_name=_('Временный')
    )

    class Meta:
        verbose_name = _('Тип сертификата')
        verbose_name_plural = _('Типы сертификатов')

    def __str__(self):
        return self.name


class AchievementType(models.Model):
    name = models.TextField(max_length=255, verbose_name=_('Название'))
    value = models.FloatField()
    is_temporal = models.BooleanField(
        default=False,
        verbose_name=_('Временный')
    )

    class Meta:
        verbose_name = _('Тип достижения')
        verbose_name_plural = _('Типы достижений')

    def __str__(self):
        return self.name


class Complaint(models.Model):
    driver = models.ForeignKey('Driver', on_delete=PROTECT)
    date = models.DateField(verbose_name=_('Дата отзыва'))
    complaint_type = models.ForeignKey('ComplaintType',
                                       verbose_name=_('Тип отзыва'),
                                       on_delete=PROTECT)
    comment = models.TextField(verbose_name=_('Комментарий'), null=True,
                               blank=True)

    class Meta:
        verbose_name = _('Отзыв на водителя')
        verbose_name_plural = _('отзывы на водителей')

        indexes = [
            models.Index(fields=['date']),
            models.Index(fields=['driver', 'date']),
        ]

    @property
    def is_temporal(self):
        return self.complaint_type.is_temporal

    @property
    def value(self):
        return self.complaint_type.value

    def __str__(self):
        return _('Отзыв') + ' #%s' % self.id


class Penalty(models.Model):
    driver = models.ForeignKey(
        'Driver',
        on_delete=PROTECT,
        null=True,
        blank=True,
        verbose_name=_('Водитель'))

    bus_park = models.ForeignKey(
        'BusPark',
        on_delete=PROTECT,
        null=True,
        blank=True,
        verbose_name=_('Автобусный парк')
    )

    date = models.DateField(
        verbose_name=_('Дата штрафа')
    )

    penalty_type = models.ForeignKey(
        'PenaltyType',
        verbose_name=_('Тип штрафа'),
        on_delete=PROTECT
    )

    comment = models.TextField(
        verbose_name=_('Комментарий'),
        null=True,
        blank=True
    )

    protocol_number = models.CharField(
        max_length=20,
        null=True,
        blank=True,
        verbose_name=_('Номер протокола')
    )

    gov_number = models.CharField(
        max_length=20,
        null=True,
        blank=True,
        verbose_name=_('Гос.номер')
    )

    preventive_measure = models.CharField(
        max_length=255,
        verbose_name=_('Мера пресечения'),
        null=True,
        blank=True
    )

    auto_model = models.CharField(
        max_length=255,
        verbose_name=_('Модель авто'),
        null=True,
        blank=True
    )

    cash_amount = models.PositiveIntegerField(
        verbose_name=_('Сумма нал.'),
        null=True,
        blank=True
    )

    recovery_amount = models.PositiveIntegerField(
        verbose_name=_('Сумма взыс.'),
        null=True,
        blank=True
    )

    class Meta:
        verbose_name = _('Штраф водителя')
        verbose_name_plural = _('Штрафы на водителей')
        indexes = [
            models.Index(fields=['driver', 'date']),
        ]

    @property
    def is_temporal(self):
        return self.penalty_type.is_temporal

    @property
    def value(self):
        return self.penalty_type.value

    def save(self, *args, **kwargs):
        if ((self.driver is None and self.bus_park is None) or
                (self.driver is not None and self.bus_park is not None)):
            raise ValueError('Должен быть указан автобусный парк или водитель')

        super().save(*args, **kwargs)

    def __str__(self):
        return _('Штраф') + ' #%s' % self.id


class Certificate(models.Model):
    driver = models.ForeignKey('Driver', on_delete=PROTECT)
    date = models.DateField(verbose_name=_('Дата получения'))
    end_date = models.DateField(verbose_name=_('Действителен до'))
    certificate_type = models.ForeignKey('CertificateType',
                                         verbose_name=_('Тип сертификата'),
                                         on_delete=PROTECT)
    certificate_number = models.CharField(max_length=255)
    comment = models.TextField(verbose_name=_('Комментарий'),
                               null=True, blank=True)

    class Meta:
        verbose_name = _('Сертификат водителя')
        verbose_name_plural = _('Сертификаты водителей')
        indexes = [
            models.Index(fields=['driver', 'date']),
        ]

    @property
    def is_temporal(self):
        return self.certificate_type.is_temporal

    @property
    def value(self):
        return self.certificate_type.value

    def __str__(self):
        return _('Сертификат') + ' #%s' % self.id


class Achievement(models.Model):
    driver = models.ForeignKey('Driver', on_delete=PROTECT)
    date = models.DateField(verbose_name=_('Дата получения'))
    achievement_type = models.ForeignKey('AchievementType',
                                         verbose_name=_('Тип достижения'),
                                         on_delete=PROTECT)
    comment = models.TextField(verbose_name=_('Комментарий'), null=True,
                               blank=True)

    class Meta:
        verbose_name = _('Достижение водителя')
        verbose_name_plural = _('Достижения водителей')
        indexes = [
            models.Index(fields=['driver', 'date']),
        ]

    @property
    def is_temporal(self):
        return self.achievement_type.is_temporal

    @property
    def value(self):
        return self.achievement_type.value

    def __str__(self):
        return _('Достижение') + ' #%s' % self.id


class UserPasswordHistory(models.Model):
    user = models.ForeignKey(User, on_delete=CASCADE)
    password = models.CharField(_('password'), max_length=128)
    created = models.DateTimeField()

    def check_password(self, password):
        return check_password(password, self.password)


class ChangeProposal(models.Model):
    driver = models.ForeignKey(Driver, verbose_name=_('Водитель'),
                               on_delete=CASCADE)
    proposal_from = models.ForeignKey(User, verbose_name=_('Создатель заявки'),
                                      on_delete=CASCADE)
    message = models.TextField(verbose_name=_('Сообщение'))
    is_read = models.BooleanField(default=False, verbose_name=_('Рассмотрено'))
    datetime = models.DateTimeField(auto_now_add=True,
                                    verbose_name=_('Дата подачи заявки'))

    class Meta:
        verbose_name = _('Заявка на изменение водителя')
        verbose_name_plural = _('Заявки на изменение водителя')


class PublicComplaint(models.Model):
    route = models.CharField(
        max_length=255,
        verbose_name=_('Маршрут')
    )

    driver = models.CharField(
        max_length=255,
        verbose_name=_('Водитель')
    )

    email = models.EmailField(
        verbose_name=_('Email'),
        null=True,
        blank=True
    )

    phone_number = models.CharField(
        max_length=20,
        verbose_name=_('Номер телефона')
    )

    message = models.TextField(
        verbose_name=_('Текст отзыва')
    )

    is_read = models.BooleanField(
        verbose_name=_('Прочитано'),
        default=False
    )

    date = models.DateTimeField(
        verbose_name=_('Дата и время комментария'),
        auto_now_add=True)

    class Meta:
        verbose_name = _('Отзыв')
        verbose_name_plural = _('Отзывы')


class JobApplication(models.Model):
    name = models.CharField(
        max_length=255,
        verbose_name=_('ФИО')
    )
    phone_number = models.CharField(
        max_length=20,
        verbose_name=_('Номер телефона')
    )
    age = models.PositiveIntegerField(
        verbose_name=_('Возраст'))
    work_exp = models.PositiveIntegerField(
        verbose_name=_('Стаж работы(лет)'))
    drive_exp = models.PositiveIntegerField(
        verbose_name=_('Стаж вождения(лет)'))
    category_d_exp = models.PositiveIntegerField(
        verbose_name=_('Стаж вождения категории D(лет)'))
    date = models.DateTimeField(
        verbose_name=_('Дата и время заявки'),
        auto_now_add=True)
    about = models.TextField(verbose_name=_('О себе'))

    is_read = models.BooleanField(
        verbose_name=_('Прочитано'),
        default=False
    )


class BusParkUser(models.Model):
    bus_park = models.ForeignKey(
        'BusPark',
        verbose_name=_('Автобусный парк'),
        on_delete=CASCADE)
    user = models.OneToOneField(
        User,
        verbose_name=_('Пользователь'),
        on_delete=CASCADE
    )

    class Meta:
        verbose_name = _('Пользователь автопарка')
        verbose_name_plural = _('Пользователи автопарка')

    def save(self, *args, **kwargs):
        if self.user:
            self.user.is_staff = True
            if not self.user.groups.filter(name=BUS_PARK_USER_GROUP_NAME).exists():
                g = Group.objects.filter(name=BUS_PARK_USER_GROUP_NAME).first()
                if g:
                    self.user.groups.add(g)
            try:
                permission = Permission.objects.filter(name__icontains="Can view Отзыв на водителя").first()
                self.user.user_permissions.add(permission)
                self.user.save()
            except:
                pass
        super().save(*args, **kwargs)

    def __str__(self):
        return _('Пользователь автопарка') + ' #%s' % self.id


class KPI(models.Model):
    driver_id = models.PositiveIntegerField()
    date_ordinal = models.IntegerField()
    value = models.FloatField()

    class Meta:
        indexes = [
            models.Index(fields=['driver_id']),
            models.Index(fields=['driver_id', 'date_ordinal']),
        ]


@receiver(post_delete, sender=DriverEmploymentHistory)
def history_delete(sender, instance, **kwargs):
    print('history delete')
    if not instance.dismissal_date:
        instance.driver.current_buspark = None
        instance.driver.save()
