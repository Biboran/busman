import datetime
import logging

import pandas
from django.db.models import Q
from celery import task
from .models import Driver, KPI, Penalty, Achievement, Certificate, Complaint, \
    DriverEmploymentHistory, BusPark

logger = logging.getLogger(__name__)


def kpi_for_event(event_date, event_value, cur_date):
    elapsed = (cur_date - event_date) / 365
    if elapsed < 0.9:
        w = 1
    elif elapsed < 3:
        w = 1 - (elapsed - 0.9) / (2 * (3 - 0.9))
    elif elapsed < 10.5:
        w = 0.5 - (elapsed - 3) / (2 * (10.5 - 3))
    else:
        w = 0
    return event_value * w


@task
def calc_kpi():
    KPI.objects.only('id').delete()
    start_date = datetime.date(2016, 1, 1)
    today = datetime.date.today()
    data = []

    for model_class in [Penalty, Achievement, Certificate, Complaint]:
        print(model_class.__name__)
        events = model_class.objects.filter(~Q(driver=None)).order_by('date')
        count = events.count()

        for i, event in enumerate(events):
            if i % 100 == 0:
                print('%s%%' % (i / float(count) * 100))
            event_date_ordinal = event.date.toordinal()
            for d in reversed(range(start_date.toordinal(), today.toordinal() + 1)):
                if event_date_ordinal > d:
                    break
                val = kpi_for_event(event_date_ordinal, event.value, d)
                data.append([float(val), int(d), int(event.driver.id)])

    df = pandas.DataFrame(data=data,
                          columns=['KPI_VAL', 'KPI_DATE', 'DRIVER_ID'])
    kpi = df.groupby(['DRIVER_ID', 'KPI_DATE'])['KPI_VAL'].sum()
    drivers = Driver.objects.all()
    count = drivers.count()
    objects = []
    for i, driver in enumerate(drivers):
        if i % 100 == 0:
            print('%s%%' % (i / float(count) * 100))

        if i % 100 == 0:
            KPI.objects.bulk_create(objects)
            objects = []

        for day in range(start_date.toordinal(), today.toordinal() + 1):
            try:
                val = kpi[int(driver.id)][int(day)]
            except:
                val = 0
            objects.append(KPI(date_ordinal=day,
                               value=val,
                               driver_id=driver.id))
    KPI.objects.bulk_create(objects)


YEARS_TO_ACCOUNT = 10
THRESHOLDS = (
    (1/6, 50),
    (1/5, 30),
    (1/3, 20),
    (1, -15),
    (2, -25)
)


def get_employment_history_modifier(driver):
    date_from = datetime.datetime.now() - datetime.timedelta(YEARS_TO_ACCOUNT * 365)
    history = DriverEmploymentHistory.objects.filter(driver=driver, employment_date__gte=date_from).exclude(dismissal_date=None)
    count = history.count()

    if count == 0:
        if DriverEmploymentHistory.objects.filter(driver=driver, dismissal_date=None).exists():
            period = min(YEARS_TO_ACCOUNT,
                 (datetime.datetime.now().date() - DriverEmploymentHistory.objects.filter(driver=driver, dismissal_date=None).latest('employment_date').employment_date).days / 365)
            if period > 6:
                return 50
            if period > 5:
                return 30
            if period > 3:
                return 20
            return 0
        else:
            return 0
    period = min(YEARS_TO_ACCOUNT,
                 (datetime.datetime.now().date() - history.earliest('employment_date').employment_date).days / 365)
    frequency = count / period

    if frequency < THRESHOLDS[0][0]:
        return THRESHOLDS[0][1]
    for i in range(1, len(THRESHOLDS)):
        if frequency < THRESHOLDS[i][0]:
            return THRESHOLDS[i-1][1] + ((frequency - THRESHOLDS[i-1][0]) / (THRESHOLDS[i][0] - THRESHOLDS[i-1][0]) * (THRESHOLDS[i][1] - THRESHOLDS[i-1][1]))
    return THRESHOLDS[-1][1]


@task
def set_current_kpi():
    today = datetime.date.today().toordinal()

    drivers = Driver.objects.all()
    count = drivers.count()
    for i, driver in enumerate(drivers):
        if i % 100 == 0:
            print('%s%%' % (i / float(count) * 100))
        kpi = KPI.objects.filter(
            driver_id=int(driver.id),
            date_ordinal=today).last()
        driver.current_kpi = get_employment_history_modifier(driver)
        if not driver.attestat_points or driver.attestat_status == 2:
            pass
        elif driver.attestat_points < 16:
            driver.current_kpi -= 15
        elif driver.attestat_points <= 20:
            driver.current_kpi += 15
        else:
            driver.current_kpi += 25
        if kpi:
            driver.current_kpi += kpi.value
        driver.save()

    for bus_park in BusPark.objects.all():
        kpis = [deh.driver.current_kpi for deh in
                DriverEmploymentHistory.objects.filter(dismissal_date=None, bus_park=bus_park)]
        if len(kpis) != 0:
            bus_park.current_kpi = sum(kpis) / len(kpis)
            bus_park.save()

# def compare_kpi():
#     from mainapp.charts import old_driver_kpi
#     start_date = datetime.date(2018, 12, 12)
#     today = datetime.date.today()
#
#     for date in date_range(start_date, today):
#         print('Calculating kpi for %s' % date)
#
#         for i, driver in enumerate(Driver.objects.all()):
#             value = old_driver_kpi(driver.id, date)
#             kpi = KPI.objects.filter(
#                 driver_id=int(driver.id),
#                 date_ordinal=date.toordinal()).first()
#             if value == 0:
#                 continue
#             if kpi:
#                 if int(value) != int(kpi.value):
#                     print(value, kpi.value)
#             else:
#                 print(value, 'none')
