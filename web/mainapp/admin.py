from django.contrib import admin
from django.contrib.admin import AdminSite
from django.db.models import Q
from django.utils.html import format_html
from django.utils.translation import gettext as _
from django.utils.translation import gettext_lazy
from django.urls import reverse
from django.utils import timezone
from django.conf import settings
from django.urls import path
from django import forms
from django.views.generic import RedirectView
from django.contrib.auth.models import User, Group
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django.db.models.fields.reverse_related import ManyToOneRel
from django.db.models.fields.related import ManyToManyField
from django.shortcuts import redirect
from django.contrib.admin.models import LogEntry
import datetime


from .views import DriverChartsView, BusParkChartsView, AlmatyChartsView, \
    upload_file

from .models import (
    Driver, BusPark, RoadSpecialist, RoadSpecialistEmploymentHistory,
    DriverEmploymentHistory,
    Complaint, ComplaintType,
    Penalty, PenaltyType,
    Certificate, CertificateType,
    Achievement, AchievementType, UserPasswordHistory, Region, Nationality,
    ChangeProposal, PublicComplaint, BusParkUser, JobApplication
)


class PermissionMixin:
    def has_change_permission(self, request, obj=None):
        bpu = BusParkUser.objects.filter(user=request.user).first()
        if bpu:
            if isinstance(obj, Driver):
                if DriverEmploymentHistory.objects.filter(bus_park=bpu.bus_park,
                                                          dismissal_date=None,
                                                          driver=obj).exists():
                    return True
                else:
                    return False
            elif isinstance(obj, DriverEmploymentHistory):
                return obj.bus_park == bpu.bus_park
        return super().has_change_permission(request, obj)

    def has_view_permission(self, request, obj=None):
        return True


def redirect_view(request):
    if request.user.is_authenticated:
        if BusParkUser.objects.filter(user=request.user).exists():
            return redirect('admin:mainapp_mydriver_changelist')
        if request.user.username=="ONAY":
            return redirect('admin:mainapp_blacklistdriver_changelist')
    return redirect('admin:mainapp_driver_changelist')


class MyAdminSite(AdminSite):
    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('', redirect_view,
                 name='index'),
            path('driver_charts/<str:chart_type>/<int:pk>/',
                 DriverChartsView.as_view(),
                 name='driver_charts'),
            path('bus_park_charts/<str:chart_type>/<int:pk>/',
                 BusParkChartsView.as_view(),
                 name='buspark_charts'),
            path('almaty_charts/<str:chart_type>/',
                 AlmatyChartsView.as_view(),
                 name='almaty_charts'),
            path('upload/', upload_file,
                 name='upload'),
        ]
        return my_urls + urls

    def admin_view(self, view, cacheable=False):
        from django.views.decorators.csrf import csrf_protect
        from django.http import HttpResponseRedirect
        from django.views.decorators.cache import never_cache
        from functools import update_wrapper
        from django.contrib import messages

        def inner(request, *args, **kwargs):
            if not self.has_permission(request):
                if request.path == reverse('admin:logout',
                                           current_app=self.name):
                    index_path = reverse('admin:index', current_app=self.name)
                    return HttpResponseRedirect(index_path)
                # Inner import to prevent django.contrib.admin (app) from
                # importing django.contrib.auth.models.User (unrelated model).
                from django.contrib.auth.views import redirect_to_login
                return redirect_to_login(
                    request.get_full_path(),
                    reverse('admin:login', current_app=self.name)
                )

            # ---- ПРОВЕРКА СРОКА ПАРОЛЯ!!! ------------------------------ #
            user = request.user
            passwords = UserPasswordHistory.objects.filter(user=user). \
                order_by('-created')

            last_password_change = passwords[0].created \
                if passwords.exists() else user.date_joined

            if (timezone.now() - last_password_change).total_seconds() > \
                    settings.USER_PASSWORD_LIFETIME:
                if '/password_change' not in request.get_full_path():
                    messages.add_message(
                        request, messages.WARNING,
                        'Ваш пароль истек. Необходимо ввести новый пароль.')
                    return HttpResponseRedirect(
                        reverse('admin:password_change'))
            # ------------------------------------------------------------ #

            return view(request, *args, **kwargs)

        if not cacheable:
            inner = never_cache(inner)
        # We add csrf_protect here so this function can be used as a utility
        # function for any view, without having to repeat 'csrf_protect'.
        if not getattr(view, 'csrf_exempt', False):
            inner = csrf_protect(inner)
        return update_wrapper(inner, view)

    def password_change(self, request, extra_context=None):
        """
        Handle the "change password" task -- both form display and validation.
        """
        from django.core.exceptions import ValidationError
        from django.contrib.auth.forms import PasswordChangeForm

        class CustomPasswordChangeForm(PasswordChangeForm):
            required_css_class = 'required'

            def clean_new_password2(self):
                # ---- ПРОВЕРКА БЫЛ ЛИ ЭТОТ ПАРОЛЬ ------------------------ #
                password2 = super().clean_new_password2()
                for uph in UserPasswordHistory.objects.filter(user=self.user):
                    if uph.check_password(password2):
                        raise ValidationError(
                            "Раннее использованные пароли не разрешены",
                            code='password_was_used',
                        )
                # --------------------------------------------------------- #
                return password2

            def save(self, commit=True):
                user = super().save(commit)
                # ----- ВЕДЕМ ИСТОРИЮ ПАРОЛЕЙ ----------------------------- #
                UserPasswordHistory.objects.create(
                    password=self.user.password, created=timezone.now(),
                    user=self.user)
                # --------------------------------------------------------- #
                return user

        from django.contrib.auth.views import PasswordChangeView
        url = reverse('admin:password_change_done', current_app=self.name)
        defaults = {
            'form_class': CustomPasswordChangeForm,
            'success_url': url,
            'extra_context': {**self.each_context(request),
                              **(extra_context or {})},
        }
        if self.password_change_template is not None:
            defaults['template_name'] = self.password_change_template
        request.current_app = self.name
        return PasswordChangeView.as_view(**defaults)(request)


class DriverEmploymentHistoryInline(PermissionMixin, admin.TabularInline):
    model = DriverEmploymentHistory
    extra = 0  # how many rows to show
    autocomplete_fields = ['bus_park']
    ordering = ('employment_date', 'id',)
    exclude = ('salary', )

    verbose_name = _('Место работы')
    verbose_name_plural = gettext_lazy('История трудоустройств')

    def get_max_num(self, request, obj=None, **kwargs):
        bpu = BusParkUser.objects.filter(user=request.user).first()

        if bpu:
            dehs = DriverEmploymentHistory.objects.filter(
                driver=obj, bus_park=bpu.bus_park)
            try:
                deh = dehs.latest('employment_date')
            except:
                return dehs.count() + 1
            if deh.dismissal_date is not None:
                return dehs.count() + 1
            return dehs.count()
        else:
            return super().get_max_num(request, obj=obj, **kwargs)


class RoadSpecialistEmploymentHistoryInline(admin.TabularInline):
    model = RoadSpecialistEmploymentHistory
    extra = 0  # how many rows to show
    autocomplete_fields = ['bus_park']

    verbose_name = gettext_lazy('Место работы')
    verbose_name_plural = gettext_lazy('История трудоустройств')


class CertificateInline(PermissionMixin, admin.TabularInline):
    model = Certificate
    extra = 0
    ordering = ('date',)

    verbose_name = gettext_lazy('Сертификат')
    verbose_name_plural = gettext_lazy('Сертификаты')


class AchievementInline(PermissionMixin, admin.TabularInline):
    model = Achievement
    extra = 0
    ordering = ('date',)

    verbose_name = gettext_lazy('Достижение')
    verbose_name_plural = gettext_lazy('Достижения')


class PenaltyDriverInline(PermissionMixin, admin.TabularInline):
    model = Penalty
    extra = 0
    exclude = ('bus_park',)
    verbose_name = gettext_lazy('Штраф')
    verbose_name_plural = gettext_lazy('Штрафы')
    ordering = ('date',)


class PenaltyBusparkInline(admin.TabularInline):
    model = Penalty
    extra = 0
    exclude = ('driver',)
    verbose_name = gettext_lazy('Штраф')
    verbose_name_plural = gettext_lazy('Штрафы')
    ordering = ('date',)


class ComplaintInline(admin.TabularInline):
    model = Complaint
    extra = 0

    verbose_name = gettext_lazy('Отзыв')
    verbose_name_plural = gettext_lazy('Отзывы')
    ordering = ('date',)


class BusParkUserInline(admin.TabularInline):
    model = BusParkUser
    autocomplete_fields = ['user']


class InputFilter(admin.SimpleListFilter):
    template = 'admin/custom_input_filter.html'

    def lookups(self, request, model_admin):
        # Dummy, required to show the filter.
        return ((),)

    def choices(self, changelist):
        # Grab only the "all" option.
        all_choice = next(super().choices(changelist))
        all_choice['query_parts'] = (
            (k, v)
            for k, v in changelist.get_filters_params().items()
            if k != self.parameter_name
        )
        yield all_choice


class KpiLessFilter(InputFilter):
    parameter_name = 'kpi_less'
    title = 'KPI <'

    def queryset(self, request, queryset):
        if self.value() is not None:
            kpi = self.value()
            try:
                kpi = int(kpi)
            except:
                return queryset
            return queryset.filter(
                current_kpi__lt=kpi
            )


class KpiGreatFilter(InputFilter):
    parameter_name = 'kpi_great'
    title = 'KPI >'

    def queryset(self, request, queryset):
        if self.value() is not None:
            kpi = self.value()
            try:
                kpi = int(kpi)
            except:
                return queryset
            return queryset.filter(
                current_kpi__gt=kpi
            )


class CurrentBusParkFilter(admin.SimpleListFilter):
    title = _('Все перевозчики Алматы')
    parameter_name = 'buspark'

    def lookups(self, request, model_admin):
        return [('none', _('Без текущего места работы') )] + [
            (bp.id, bp.name) for bp in BusPark.objects.all().order_by('name')
        ]

    def queryset(self, request, queryset):
        bus_park_id = self.value()
        if bus_park_id:
            if bus_park_id == 'none':
                driver_ids = [d.id for d in
                              Driver.objects.filter(current_buspark=None)]
            else:
                driver_ids = [deh.driver_id for deh in
                              DriverEmploymentHistory.objects.filter(
                                  dismissal_date=None,
                                  bus_park_id=bus_park_id)]

            return queryset.filter(id__in=driver_ids)
        return queryset


class AgeFilter(admin.SimpleListFilter):
    title = _('Любой возраст')
    parameter_name = 'birth_date'

    def lookups(self, request, model_admin):
        return [
            ("{}-{}".format(i, i + 9), _("{}-{} лет".format(i, i+9))) for i in range(20, 70, 10)
        ]

    def queryset(self, request, queryset):
        age_interval = self.value()
        if age_interval:
            age_from, age_to = (int(i) for i in age_interval.split("-"))
            queryset = queryset.filter(birth_date__lte=timezone.now() - datetime.timedelta(days=age_from*365),
                                       birth_date__gte=timezone.now() - datetime.timedelta(days=age_to*365))
        return queryset


class BusParkAdmin(admin.ModelAdmin):
    model = BusPark
    inlines = [PenaltyBusparkInline, BusParkUserInline]
    list_display = ['__str__', 'count_drivers', 'current_kpi', '_menu']
    search_fields = ['name']

    def _menu(self, obj):
        detail_url = reverse('admin:buspark_charts', args=['scatter', obj.id])
        change_url = reverse('admin:mainapp_buspark_change', args=[obj.id])
        l1 = '<a class="btn btn-primary" href="{}">%s</a> ' % _(
            'Отчет по автопарку')
        l2 = '<a class="btn btn-primary" href="{}">%s</a>' % _('Редактировать')
        return format_html(l1, detail_url) + format_html(l2, change_url)

    def count_drivers(self, obj):
        return DriverEmploymentHistory.objects.filter(dismissal_date=None,
                                                      bus_park=obj).count()

    _menu.short_description = ''
    count_drivers.short_description = _('Кол-во водителей')


class DriverAdmin(PermissionMixin, admin.ModelAdmin):
    inlines = [DriverEmploymentHistoryInline,
               CertificateInline, AchievementInline,
               PenaltyDriverInline, ComplaintInline]
    list_display = ['last_name', 'first_name', 'patronymic', 'iin',
                    'current_kpi', 'registration_region',
                    'current_bus_park',
                    '_menu']
    search_fields = ['first_name', 'last_name', 'patronymic', 'id_card_number',
                     'iin']
    list_filter = ['registration_region', CurrentBusParkFilter, AgeFilter, KpiGreatFilter,
                   KpiLessFilter]
    ordering = ('last_name',)
    # exclude = ('current_kpi',)
    readonly_fields = ('current_buspark',)

    def _menu(self, obj):
        charts_url = reverse('admin:driver_charts', args=['scatter', obj.id])
        change_url = reverse('admin:mainapp_driver_change', args=[obj.id])
        l1 = '<a class="btn btn-primary" href="{}">%s</a> ' % _(
            'Отчет по водителю')
        l2 = '<a class="btn btn-primary" href="{}">%s</i></a>' \
             % _('Просмотреть личное дело')
        return format_html(l2, change_url) + format_html(l1, charts_url)

    _menu.short_description = ''

    def current_bus_park(self, obj):
        deh = DriverEmploymentHistory.objects.filter(dismissal_date=None,
                                                     driver=obj)
        if deh.exists():
            return deh[0].bus_park
        return None

    def get_queryset(self, request):
        if request.user.is_superuser:
            DriverAdmin.fieldsets = (
                ('', {
                    'fields': (
                        'photo',
                        'cv',
                        'last_name',
                        'first_name',
                        'patronymic',
                        'birth_date',
                        'gender',
                        'nationality',
                        'registration_address',
                        'registration_region',

                    )
                }),
                ('', {
                    'fields': (
                        'id_card_number',
                        'id_card_issue_date',
                        'iin',
                        'work_start_date',
                        'phone_number',
                        'attestat_status',
                        'attestat_points',
                        'attestat_date',
                        'current_buspark',
                        'route_number',
                        'current_kpi',
                    )
                }),
            )
        else:
            DriverAdmin.fieldsets = (
                ('', {
                    'fields': (
                        'photo',
                        'cv',
                        'last_name',
                        'first_name',
                        'patronymic',
                        'birth_date',
                        'gender',
                        'nationality',
                        'registration_address',
                        'registration_region',

                    )
                }),
                ('', {
                    'fields': (
                        'id_card_number',
                        'id_card_issue_date',
                        'iin',
                        'work_start_date',
                        'attestat_status',
                        'attestat_points',
                        'attestat_date',
                        'current_buspark',
                        'route_number',
                        'current_kpi',
                    )
                }),
            )

        qs = super().get_queryset(request)

        # bpu = BusParkUser.objects.filter(user=request.user).first()
        # if bpu:
        #     drivers = [deh.driver_id for deh in
        #                DriverEmploymentHistory.objects.filter(
        #                    bus_park=bpu.bus_park, dismissal_date=None)]
        #     return qs.filter(id__in=drivers)
        return qs

    def get_fields(self, request, obj=None):
        bpu = BusParkUser.objects.filter(user=request.user).first()
        if bpu:
            return [f.name for f in Driver._meta.get_fields()
                    if (not isinstance(f, (ManyToOneRel, ManyToManyField))
                        and f.name != 'id')]
        return super().get_fields(request, obj)

    def get_readonly_fields(self, request, obj=None):
        bpu = BusParkUser.objects.filter(user=request.user).first()
        if bpu:
            fields = [f.name for f in Driver._meta.get_fields()
                      if not isinstance(f, (ManyToOneRel, ManyToManyField))]
            editable = ['phone_number', 'registration_address',
                        'registration_region',
                        'id_card_number', 'id_card_issue_entity',
                        'id_card_issue_date', 'iin', 'route_number',]
            return [f for f in fields if f not in editable]
        res = super().get_readonly_fields(request, obj)
        return res + ('current_kpi',)

    current_bus_park.short_description = gettext_lazy('Текущее место работы')


class RoadSpecialistAdmin(admin.ModelAdmin):
    inlines = [RoadSpecialistEmploymentHistoryInline]


class SearchByDriverAdmin(admin.ModelAdmin):
    search_fields = ['driver__first_name', 'driver__last_name',
                     'driver__patronymic', 'driver__iin']
    autocomplete_fields = ['driver']

    def iin(self, obj):
        if obj.driver is not None:
            return obj.driver.iin

    iin.short_description = _('ИИН')


class ComplaintAdmin(SearchByDriverAdmin):
    list_display = ['driver', 'complaint_type', 'date', 'iin',
                    'comment']
    list_filter = ['complaint_type']


class PenaltyForm(forms.ModelForm):
    model = Penalty

    def clean(self):
        c = super().clean()
        driver = c.get('driver')
        bus_park = c.get('bus_park')
        if ((driver is None and bus_park is None) or (
                driver is not None and bus_park is not None)):
            raise forms.ValidationError('Должен быть указан парк или водитель')


class BusparkComplaint(Complaint):
    class Meta:
        proxy = True


class BusparkComplaintAdmin(ComplaintAdmin):
    # list_filter = ['registration_region', CurrentBusParkFilter, AgeFilter]
    # list_display_links = None
    #
    # list_display = ['__str__', 'iin', 'current_kpi', 'registration_region', 'current_bus_park', '_menu']

    class Meta:
        proxy = True

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        try:
            return qs.filter(driver__current_buspark=request.user.busparkuser.bus_park)
        except:
            return qs

    # def has_add_permission(self, request, obj=None):
    #     return True

    def has_view_permission(self, request, obj=None):
        return True


class PenaltyAdmin(SearchByDriverAdmin):
    list_display = ['driver', 'penalty_type', 'date', 'iin',
                    'comment']
    list_filter = ['penalty_type']
    form = PenaltyForm

    def get_queryset(self, request):
        return super().get_queryset(request).filter(bus_park=None)


class BusparkPenalty(Penalty):
    class Meta:
        proxy = True


class BusparkPenaltyAdmin(PenaltyAdmin):
    # list_filter = ['registration_region', CurrentBusParkFilter, AgeFilter]
    # list_display_links = None
    #
    # list_display = ['__str__', 'iin', 'current_kpi', 'registration_region', 'current_bus_park', '_menu']

    class Meta:
        proxy = True

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        try:
            return qs.filter(Q(driver__current_buspark=request.user.busparkuser.bus_park) | Q(bus_park=request.user.busparkuser.bus_park))
        except:
            return qs

    # def has_add_permission(self, request, obj=None):
    #     return True

    def has_view_permission(self, request, obj=None):
        return True


class AchievementAdmin(SearchByDriverAdmin):
    list_display = ['driver', 'achievement_type', 'date', 'iin',
                    'comment']
    list_filter = ['achievement_type']


class BusparkAchievement(Achievement):
    class Meta:
        proxy = True


class BusparkAchievementAdmin(AchievementAdmin):
    # list_filter = ['registration_region', CurrentBusParkFilter, AgeFilter]
    # list_display_links = None
    #
    # list_display = ['__str__', 'iin', 'current_kpi', 'registration_region', 'current_bus_park', '_menu']

    class Meta:
        proxy = True

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        try:
            return qs.filter(driver__current_buspark=request.user.busparkuser.bus_park)
        except:
            return qs

    # def has_add_permission(self, request, obj=None):
    #     return True

    def has_view_permission(self, request, obj=None):
        return True


class CertificateAdmin(SearchByDriverAdmin):
    list_display = ['driver', 'certificate_type', 'certificate_number', 'date',
                    'end_date', 'iin', 'comment']
    list_filter = ['certificate_type', 'end_date']


class BusparkCertificate(Certificate):
    class Meta:
        proxy = True


class BusparkCertificateAdmin(CertificateAdmin):
    # list_filter = ['registration_region', CurrentBusParkFilter, AgeFilter]
    # list_display_links = None
    #
    # list_display = ['__str__', 'iin', 'current_kpi', 'registration_region', 'current_bus_park', '_menu']

    class Meta:
        proxy = True

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        try:
            return qs.filter(driver__current_buspark=request.user.busparkuser.bus_park)
        except:
            return qs

    # def has_add_permission(self, request, obj=None):
    #     return True

    def has_view_permission(self, request, obj=None):
        return True


class ChangeProposalAdmin(admin.ModelAdmin):
    list_display = ['driver', 'proposal_from', 'message', 'is_read', 'datetime']
    autocomplete_fields = ['driver']
    list_filter = ['is_read']


class PublicComplaintAdmin(admin.ModelAdmin):
    list_display = ['driver', 'route', 'is_read', 'message', 'email',
                    'phone_number']
    list_filter = ['is_read']


class JobApplicationAdmin(admin.ModelAdmin):
    list_display = ['name', 'is_read', 'age', 'work_exp', 'drive_exp',
                    'category_d_exp',
                    'phone_number']
    list_filter = ['is_read']


class MyDriver(Driver):
    class Meta:
        proxy = True
        verbose_name = _('Мой водитель')
        verbose_name_plural = _('Мои водители')


class MyDriverAdmin(DriverAdmin):
    list_filter = ['registration_region', KpiGreatFilter, KpiLessFilter, AgeFilter]

    def get_queryset(self, request):
        qs = super().get_queryset(request)

        bpu = BusParkUser.objects.filter(user=request.user).first()
        if bpu:
            drivers = [deh.driver_id for deh in
                       DriverEmploymentHistory.objects.filter(bus_park=bpu.bus_park, dismissal_date=None)]
            return qs.filter(id__in=drivers)
        else:
            return qs.none()


class BlackListDriver(Driver):
    class Meta:
        proxy = True
        verbose_name = _('Черный список')
        verbose_name_plural = _('Черный список')


class BlackListDriverAdmin(DriverAdmin):
    list_filter = ['registration_region', CurrentBusParkFilter, AgeFilter]
    list_display_links = None

    list_display = ['__str__', 'iin', 'phone_number', 'current_kpi', 'registration_region', 'current_bus_park', '_menu']

    list_per_page = 100

    class Meta:
        proxy = True

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(current_kpi__lte=-200)

    def has_add_permission(self, request, obj=None):
        return False


class RiskListDriver(Driver):
    class Meta:
        proxy = True
        verbose_name = _('Зона риска')
        verbose_name_plural = _('Зона риска')


class RiskListDriverAdmin(DriverAdmin):
    list_filter = ['registration_region', CurrentBusParkFilter, AgeFilter]
    list_display_links = None

    list_display = ['__str__', 'iin', 'phone_number', 'current_kpi', 'registration_region', 'current_bus_park', '_menu']

    list_per_page = 100

    class Meta:
        proxy = True

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(current_kpi__lte=-20, current_kpi__gte=-199)

    def has_add_permission(self, request, obj=None):
        return False

    def has_view_permission(self, request, obj=None):
        return True


class UnemployedDriver(Driver):
    class Meta:
        proxy = True
        verbose_name = _('Кадровый резерв')
        verbose_name_plural = _('Кадровый резерв')


class UnemployedDriverAdmin(DriverAdmin):
    list_filter = ['registration_region', AgeFilter]
    list_display_links = None

    list_display = ['__str__', 'iin', 'phone_number', 'current_kpi', 'registration_region', 'current_bus_park', '_menu']

    class Meta:
        proxy = True

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(current_buspark=None, current_kpi__gte=-199)

    def has_add_permission(self, request, obj=None):
        return False

    def has_view_permission(self, request, obj=None):
        return True


class LogEntryAdmin(admin.ModelAdmin):
    list_filter = ['user']
    list_display = ['action_time', 'user', 'content_type', 'object_id',
                    'object_repr', 'change_message']


admin_site = MyAdminSite(name='myadmin')

admin_site.register(Driver, DriverAdmin)
admin_site.register(BlackListDriver, BlackListDriverAdmin)
admin_site.register(RiskListDriver, RiskListDriverAdmin)
admin_site.register(UnemployedDriver, UnemployedDriverAdmin)
admin_site.register(MyDriver, MyDriverAdmin)
admin_site.register(BusPark, BusParkAdmin)
admin_site.register(RoadSpecialist, RoadSpecialistAdmin)

admin_site.register(Complaint, ComplaintAdmin)
admin_site.register(BusparkComplaint, BusparkComplaintAdmin)
admin_site.register(Penalty, PenaltyAdmin)
admin_site.register(BusparkPenalty, BusparkPenaltyAdmin)
admin_site.register(Achievement, AchievementAdmin)
admin_site.register(BusparkAchievement, BusparkAchievementAdmin)
admin_site.register(Certificate, CertificateAdmin)
admin_site.register(BusparkCertificate, BusparkCertificateAdmin)

admin_site.register(ComplaintType)
admin_site.register(PenaltyType)
admin_site.register(CertificateType)
admin_site.register(AchievementType)

admin_site.register(Region)
admin_site.register(Nationality)
admin_site.register(ChangeProposal, ChangeProposalAdmin)
admin_site.register(PublicComplaint, PublicComplaintAdmin)
admin_site.register(JobApplication, JobApplicationAdmin)

admin_site.register(User, UserAdmin)
admin_site.register(Group, GroupAdmin)
admin_site.register(LogEntry, LogEntryAdmin)
