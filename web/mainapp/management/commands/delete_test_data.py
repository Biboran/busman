from django.core.management.base import BaseCommand
from django.db import transaction

from mainapp.models import *


class Command(BaseCommand):

    @transaction.atomic
    def handle(self, *args, **options):
        DriverEmploymentHistory.objects.filter(
            driver__iin='123123123123').delete()

        Complaint.objects.filter(driver__iin='123123123123').delete()
        Penalty.objects.filter(driver__iin='123123123123').delete()
        Certificate.objects.filter(driver__iin='123123123123').delete()
        Achievement.objects.filter(driver__iin='123123123123').delete()
        BusPark.objects.filter(
            name__in=['Парк #1', 'Парк #2', 'Парк #3', 'Парк #4',
                      'Парк #5']).delete()
        Driver.objects.filter(iin='123123123123').delete()
