import random
import string
from datetime import datetime

from django.core.management.base import BaseCommand
from django.db import transaction

from mainapp.models import *

random.seed(0)
DRIVER_COUNT = 20
BUS_PARK_COUNT = 5

MAX_JOBS_PER_DRIVER = 3

TYPE_COUNT = 3
MAX_ELEMENT_COUNT = 5


class Command(BaseCommand):
    def clear_tables(self):
        for m in [DriverEmploymentHistory,
                  RoadSpecialistEmploymentHistory, Complaint, Penalty,
                  Certificate, Achievement, ComplaintType, PenaltyType,
                  AchievementType, CertificateType, Driver, BusPark,
                  RoadSpecialist, Nationality, Region]:
            m.objects.all().delete()

    @transaction.atomic
    def handle(self, *args, **options):
        self.clear_tables()
        first_names = ['Петя', 'Вася', 'Витя', 'Женя', 'Ваня']
        last_names = ['Орлов', 'Коробков', 'Комаров', 'Грушин', 'Ковалев']
        patronymics = ['Петрович', 'Васильевич', 'Витальевич', 'Евгеньевич',
                       'Иванович']
        birth_date = datetime(1985, 1, 1)
        work_start_date = datetime(2000, 1, 1)
        gender = 1
        nationality = Nationality.objects.create(name='Русский')
        nationality = Nationality.objects.create(name='Казах')
        id_card_issue_entity = 'МИН.ЮСТ.'
        id_card_issue_date = datetime(2010, 1, 1)

        registration_address = 'Пушкина 1, 133'
        regions = [Region.objects.create(name='Алматы'),
                   Region.objects.create(name='Шымкент')]

        for i in range(DRIVER_COUNT):
            Driver.objects.create(first_name=random.choice(first_names),
                                  last_name=random.choice(last_names),
                                  patronymic=random.choice(patronymics),
                                  birth_date=birth_date,
                                  work_start_date=work_start_date,
                                  gender=gender,
                                  iin=''.join(random.choices(string.digits, k=12)),
                                  nationality=nationality,
                                  id_card_number=''.join(random.choices(string.digits, k=9)),
                                  id_card_issue_entity=id_card_issue_entity,
                                  id_card_issue_date=id_card_issue_date,
                                  registration_address=registration_address,
                                  registration_region=random.choice(
                                      regions),
                                  phone_number='+77771231231')

        for i in range(BUS_PARK_COUNT):
            BusPark.objects.create(name=f'Парк #{i + 1}',
                                   address='Фурманова 123',
                                   phone_number='7777777')

        bus_parks = BusPark.objects.all().order_by('id')

        for i in range(TYPE_COUNT):
            ComplaintType.objects.create(name=f'COMPLAINT_TYPE_#{i + 1}',
                                         value=-i - 1)

            PenaltyType.objects.create(name=f'PENALTY_TYPE_#{i + 1}',
                                       value=-i - 1)
            AchievementType.objects.create(name=f'ACHIEVEMENT_TYPE_#{i + 1}',
                                           value=i + 1)
            CertificateType.objects.create(name=f'CERTIFICATE_TYPE_#{i + 1}',
                                           value=i + 1)

        complaint_types = ComplaintType.objects.all().order_by('id')
        achievement_types = AchievementType.objects.all().order_by('id')
        penalty_types = PenaltyType.objects.all().order_by('id')
        certificate_types = CertificateType.objects.all().order_by('id')

        for driver in Driver.objects.all().order_by('id'):

            job_count = random.randint(1, MAX_JOBS_PER_DRIVER + 1)

            prev_year = 2000
            prev_bus_park = None
            for i in range(job_count):
                bus_park = random.choice(bus_parks)

                while bus_park == prev_bus_park:
                    bus_park = random.choice(bus_parks)

                prev_bus_park = bus_park
                employment_date = datetime(prev_year + random.randint(1, 3),
                                           1, 1)
                if i != job_count - 1:
                    dismissal_date = datetime(
                        employment_date.year + random.randint(1, 3), 1, 1)
                    prev_year = dismissal_date.year
                else:
                    dismissal_date = None

                salary = random.randint(80, 200) * 1000
                remarks = '-'
                dismissal_cause = 'hz'

                DriverEmploymentHistory.objects.create(
                    bus_park=bus_park,
                    driver=driver,
                    employment_date=employment_date,
                    dismissal_date=dismissal_date,
                    salary=salary,
                    dismissal_cause=dismissal_cause,
                    remarks=remarks)

                for j in range(random.randint(1, MAX_ELEMENT_COUNT + 1)):
                    if dismissal_date:
                        y = random.randint(employment_date.year,
                                           dismissal_date.year + 1)
                    else:
                        y = employment_date.year
                    Complaint.objects.create(
                        driver=driver,
                        complaint_type=random.choice(
                            complaint_types),
                        date=datetime(y, 2, 2)
                    )

                for j in range(random.randint(1, MAX_ELEMENT_COUNT + 1)):
                    if dismissal_date:
                        y = random.randint(employment_date.year,
                                           dismissal_date.year + 1)
                    else:
                        y = employment_date.year
                    Penalty.objects.create(
                        driver=driver,
                        penalty_type=random.choice(
                            penalty_types),
                        date=datetime(y, 2, 2)
                    )

                for j in range(random.randint(1, MAX_ELEMENT_COUNT + 1)):
                    if dismissal_date:
                        y = random.randint(employment_date.year,
                                           dismissal_date.year + 1)
                    else:
                        y = employment_date.year
                    Certificate.objects.create(
                        driver=driver,
                        certificate_type=random.choice(
                            certificate_types),
                        date=datetime(y, 2, 2),
                        end_date=datetime(y, 3, 3),

                        certificate_number='000',
                        comment='-')

                for j in range(random.randint(1, MAX_ELEMENT_COUNT + 1)):
                    if dismissal_date:
                        y = random.randint(employment_date.year,
                                           dismissal_date.year + 1)
                    else:
                        y = employment_date.year
                    Achievement.objects.create(
                        driver=driver, achievement_type=random.choice(
                            achievement_types),
                        date=datetime(y, 2, 2),
                        comment='-')
