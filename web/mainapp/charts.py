import datetime
import calendar
from bisect import bisect_left
from collections import defaultdict, OrderedDict

from django.db.models import Q
from django.utils.translation import gettext as _

from django.db.models import Avg, F, Max, Min
import plotly.graph_objs as go
from plotly.offline import plot

from mainapp.models import DriverEmploymentHistory, \
    Penalty, Complaint, Certificate, Achievement, Region, KPI
from mainapp.utils import timeit

GRANULARITY_NAMES = [_('Год'), _('Месяц'), _('Неделя'), _('День')]
MODEL_NAMES = [_('Штрафы'), _('Замечания'), _('Достижения'), _('Сертификаты')]


def date_range(start_date, end_date):
    for ordinal in range(start_date.toordinal(), end_date.toordinal() + 1):
        yield datetime.date.fromordinal(ordinal)


def next_weekday(d, weekday):
    days_ahead = weekday - d.weekday()
    if days_ahead <= 0:
        days_ahead += 7
    return d + datetime.timedelta(days_ahead)


def render_chart(caption, x_axis_name, y_axis_name, data,
                 width=800, height=400, _type='scatter'):
    x_data, y_data = zip(*data)
    if _type == 'scatter':
        trace1 = go.Scatter(
            x=x_data,
            y=y_data
        )
    elif _type == 'bar':
        trace1 = go.Bar(
            x=x_data,
            y=y_data
        )
    elif _type == 'pie':
        trace1 = go.Pie(
            labels=x_data,
            values=y_data
        )

    data = [trace1]

    layout = go.Layout(
        title=caption,
        autosize=False,
        width=width,
        height=height,

        xaxis=dict(
            title=x_axis_name,
            autorange=True
        ),
        yaxis=dict(
            title=y_axis_name,
            autorange=True
        )
    )

    if _type == 'pie':
        layout = go.Layout(
            title=caption,
            autosize=False,
            width=width,
            height=height)

    fig = go.Figure(data=data, layout=layout)

    config = {
        'showLink': False,
        'displaylogo': False,
        'modeBarButtonsToRemove': [
            'sendDataToCloud',
            'hoverCompareCartesian',
            'hoverClosestCartesian',
            'toggleSpikelines',
            'lasso2d',
            'select2d'
        ]
    }

    plot_div = plot(fig, output_type='div', include_plotlyjs=False,
                    config=config)
    return plot_div


def filter_dates(dates, sd, ed):
    sd = sd.toordinal()
    if ed:
        ed = ed.toordinal()
    else:
        ed = datetime.date(3000, 1, 1).toordinal()

    # i, j = len(dates), len(dates)
    # for pos, x in enumerate(dates):
    #     if x >= sd:
    #         i = pos
    #         break
    # for pos, x in enumerate(dates):
    #     if x > ed:
    #         j = pos
    #         break
    # return i,j

    i = bisect_left(dates, sd)
    j = bisect_left(dates, ed)
    if len(dates) != j and ed == dates[j]:
        for x in range(j, len(dates)):
            if dates[x] != ed:
                j = x
                break
        else:
            j = len(dates)
    return i, j


def count_dates(dates, start_date, end_date):
    i, j = filter_dates(dates, start_date, end_date)
    return j - i


@timeit
def select_drivers_dates(model_class, dehs, from_date, to_date):
    objects = model_class.objects.filter(
        driver_id__in=[deh.driver_id for deh in dehs],
        date__gte=from_date,
        date__lte=to_date
    ).order_by('driver_id', 'date')

    data = defaultdict(list)
    for o in objects:
        data[o.driver_id].append(o.date.toordinal())

    dates = []

    for deh in dehs:
        _dates = data[deh.driver_id]
        i, j = filter_dates(_dates, deh.employment_date, deh.dismissal_date)
        dates += _dates[i:j]
    return sorted(dates)


@timeit
def granulated_dates(from_date, to_date, granularity):
    today = datetime.date.today()

    to_date = min(today, to_date)

    dates = []
    if granularity == 1:
        for year in range(from_date.year, to_date.year + 1):
            sd = datetime.date(year, 1, 1)
            if year != today.year:
                ed = datetime.date(year, 12, calendar.monthrange(year, 12)[1])
            else:
                ed = today
            dates.append((sd, ed, str(sd)))
    if granularity == 2:
        for year in range(from_date.year, to_date.year + 1):
            for month in range(1, 13):
                sd = datetime.date(year, month, 1)
                if sd > to_date:
                    break
                if year == today.year and month == today.month:
                    ed = today
                else:
                    ed = datetime.date(year, month,
                                       calendar.monthrange(year, month)[1])
                dates.append((sd, ed, str(sd)))
    if granularity == 3:
        d = from_date
        while d < to_date:
            sd = next_weekday(d, 0)
            ed = sd + datetime.timedelta(days=6)
            if ed > today:
                ed = today
            dates.append((sd, ed, str(sd)))
            d = sd
    if granularity == 4:
        for d in date_range(from_date, to_date):
            dates.append((d, d, str(d)))
    return dates


@timeit
def bus_park_line_chart(model_class, title, bus_park_id, from_date,
                        to_date, granularity, cumulative, region):
    label = GRANULARITY_NAMES[granularity - 1]

    if bus_park_id is None:
        dehs = DriverEmploymentHistory.objects.all()  # can be optimized?
    else:
        dehs = DriverEmploymentHistory.objects.filter(bus_park_id=bus_park_id)
    if region != 0:
        dehs = dehs.filter(driver__registration_region=region)

    dates = select_drivers_dates(model_class, dehs, from_date, to_date)

    data = OrderedDict()

    for sd, ed, key in granulated_dates(from_date, to_date, granularity):
        data[key] = count_dates(dates, sd, ed)

    if cumulative:
        sum = 0
        for key in data:
            sum += data[key]
            data[key] = sum

    return render_chart(title, label, _('Количество'), data.items())


@timeit
def driver_line_chart(model_class, title, driver_id, from_date,
                      to_date, granularity, cumulative):
    label = GRANULARITY_NAMES[granularity - 1]

    dehs = DriverEmploymentHistory.objects.filter(driver_id=driver_id)

    dates = select_drivers_dates(model_class, dehs, from_date, to_date)

    data = OrderedDict()

    for sd, ed, key in granulated_dates(from_date, to_date, granularity):
        data[key] = count_dates(dates, sd, ed)

    if cumulative:
        sum = 0
        for key in data:
            sum += data[key]
            data[key] = sum

    return render_chart(title, label, _('Количество'), data.items())


FORGET_YEARS = [0.9, 3, 10.5]


def driver_kpi(driver_id, to_date):
    try:
        kpi = KPI.objects.get(driver_id=driver_id,
                              date_ordinal=to_date.toordinal())
        return kpi.value
    except KPI.DoesNotExist:
        return 0


def old_driver_kpi(driver_id, to_date):
    res = 0
    for model_class, type_field in zip(
            [Penalty, Complaint, Certificate, Achievement],
            ['penalty_type', 'complaint_type', 'certificate_type',
             'achievement_type']):

        qs = model_class.objects.filter(driver_id=driver_id,
                                        date__lt=to_date).select_related(
            type_field)

        for p in qs:
            elapsed = (to_date - p.date).days / 365
            if p.is_temporal:
                weight = 0
                for i, fy in enumerate(FORGET_YEARS):
                    if elapsed < fy:
                        if i == 0:
                            weight = 1
                        else:
                            pfy = FORGET_YEARS[i - 1]
                            weight = 1 - (elapsed - pfy) / (fy - pfy) * 0.5
                        break
            else:
                weight = 1
            res += p.value * weight
    return res


@timeit
def driver_kpi_chart(driver_id, from_date, to_date, granularity, cumulative):
    data = OrderedDict()
    x_label = GRANULARITY_NAMES[granularity - 1]

    for sd, ed, key in granulated_dates(from_date, to_date, granularity):
        data[key] = driver_kpi(driver_id, ed)

    return render_chart(_('KPI водителя'), x_label, _('KPI'),
                        data.items())


@timeit
def get_bus_park_drivers(bus_park_id, from_date, to_date, region):
    dehs = DriverEmploymentHistory.objects.filter(
        Q(bus_park_id=bus_park_id) &
        Q(employment_date__lte=to_date) &
        (Q(dismissal_date__gte=from_date) | Q(dismissal_date=None))
    )
    if region != 0:
        dehs = dehs.filter(driver__registration_region=region)
    return dehs


def get_almaty_drivers(from_date, to_date, region):
    dehs = DriverEmploymentHistory.objects.filter(
        Q(employment_date__lte=to_date) &
        (Q(dismissal_date__gte=from_date) | Q(dismissal_date=None))
    )
    if region != 0:
        dehs = dehs.filter(driver__registration_region=region)
    return dehs


def drivers_selection(bus_park_id, start_date, end_date, param, region):
    end_date_time = datetime.datetime(end_date.year, end_date.month,
                                      end_date.day, 0, 0)
    if param in ['count', 'salary', 'age', 'experience', 'kpi']:
        if bus_park_id is None:
            dehs = get_almaty_drivers(start_date, end_date, region)
        else:
            dehs = get_bus_park_drivers(bus_park_id, start_date, end_date,
                                        region)

        if param == 'count':
            return dehs.count()
        if param == 'salary':
            if len(dehs) == 0:
                return 0
            return list(dehs.aggregate(Avg('salary')).values())[0]
        if param == 'age':
            if len(dehs) == 0:
                return 0
            return list(dehs.annotate(age=(
                    end_date_time - F(
                'driver__birth_date'))
            ).aggregate(Avg('age')).values())[0].days / 365
        if param == 'experience':
            if len(dehs) == 0:
                return 0
            return list(dehs.annotate(experience=(
                    end_date_time - F('driver__work_start_date'))
            ).aggregate(Avg('experience')).values())[0].days / 365
        if param == 'kpi':
            if len(dehs) == 0:
                return 0

            # optimized
            kpis = [k.value for k in KPI.objects.filter(
                driver_id__in=[d.driver_id for d in dehs],
                date_ordinal=end_date.toordinal())]
            return float(
                sum(kpis) / len(dehs)
            )
            # not optimized
            # return float(
            #     sum(driver_kpi(deh.driver_id, end_date) for deh in dehs)
            # ) / len(dehs)

    if param == 'dismissal':
        if bus_park_id is None:
            dehs = DriverEmploymentHistory.objects.filter(
                Q(dismissal_date__gte=start_date) & Q(
                    dismissal_date__lte=end_date))
        else:

            dehs = DriverEmploymentHistory.objects.filter(
                Q(bus_park_id=bus_park_id) &
                Q(dismissal_date__gte=start_date) & Q(
                    dismissal_date__lte=end_date))
        if region != 0:
            dehs = dehs.filter(driver__registration_region=region)

        return dehs.count()


@timeit
def bus_park_drivers_chart(bus_park_id, title, y_title, param, from_date,
                           to_date, granularity, region):
    label = GRANULARITY_NAMES[granularity - 1]
    data = OrderedDict()

    for sd, ed, key in granulated_dates(from_date, to_date, granularity):
        data[key] = drivers_selection(bus_park_id, sd, ed, param,
                                      region)
    return render_chart(title, label, y_title, data.items())


@timeit
def driver_points_bar_chart(driver_id, from_date, to_date):
    model_classes = [Penalty, Complaint, Achievement, Certificate]
    data = []
    for m, n in zip(model_classes, MODEL_NAMES):
        count = m.objects.filter(driver_id=driver_id, date__gte=from_date,
                                 date__lte=to_date).count()
        data.append((n, count))
    return render_chart('', '', _('Количество'), data, _type='bar')


@timeit
def bus_park_points_bar_chart(bus_park_id, from_date, to_date, region):
    model_classes = [Penalty, Complaint, Achievement, Certificate]

    data = []

    if bus_park_id is None:
        dehs = DriverEmploymentHistory.objects.filter()
    else:
        dehs = DriverEmploymentHistory.objects.filter(bus_park_id=bus_park_id)
    if region != 0:
        dehs = dehs.filter(driver__registration_region=region)

    for m, n in zip(model_classes, MODEL_NAMES):
        count = len(select_drivers_dates(m, dehs, from_date, to_date))
        data.append((n, count))
    return render_chart('', '', _('Количество'), data, _type='bar')


@timeit
def bus_park_range_bar_chart(bus_park_id, from_date, to_date, region, param):
    if bus_park_id is None:
        dehs = get_almaty_drivers(from_date, to_date, region)
    else:
        dehs = get_bus_park_drivers(bus_park_id, from_date, to_date, region)

    ranges = {
        'age': ((18, 20), (21, 30), (31, 40), (41, 50), (51, 60), (61, 100)),
        'experience': (
            (0, 1), (2, 5), (6, 10), (11, 20), (21, 30), (31, 50), (51, 100)),
        'salary': (
            (30, 50), (50, 70), (70, 100), (100, 150), (150, 250), (250, 500)),
        'kpi': [],
    }[param]
    title, x_label = {
        'age': (_('Возраст'), _('Возраст')),
        'experience': (_('Опыт работы'), _('Опыт работы')),
        'salary': (_('Зарплата'), _('Тыс. тенге')),
        'kpi': (_('KPI'), _('KPI')),

    }[param]

    if param == 'kpi':
        all_kpi = {}

        driver_ids = [int(deh.driver_id) for deh in dehs]

        kpis = KPI.objects.filter(driver_id__in=driver_ids,
                                  date_ordinal=to_date.toordinal())

        min_kpi = kpis.aggregate(Min('value'))['value__min']
        max_kpi = kpis.aggregate(Max('value'))['value__max']

        for kpi in kpis:
            all_kpi[kpi.driver_id] = kpi.value

        if max_kpi is not None:
            step = int((max_kpi - min_kpi) / 5)

            for i in range(int(min_kpi), int(max_kpi), step):
                ranges.append((int(i), int(i + step)))

    data = OrderedDict()
    for r in ranges:
        data[str(r)] = 0
    for deh in dehs:
        if param == 'age':
            v = (datetime.date.today() - deh.driver.birth_date).days / 365
        if param == 'experience':
            v = (datetime.date.today() - deh.driver.work_start_date).days / 365
        if param == 'salary':
            v = deh.salary
        if param == 'kpi':
            try:
                v = all_kpi[deh.driver_id]
            except KeyError:
                v = 0
        for r in ranges:
            if param == 'salary':
                if r[0] * 1000 + 1 <= v <= r[1] * 1000:
                    data[str(r)] += 1
                    break
            else:
                if r[0] <= v <= r[1]:
                    data[str(r)] += 1
                    break
    if len(data) == 0:
        data['0'] = 0
    return render_chart(title, x_label, _('Количество'), data.items(),
                        _type='bar')


@timeit
def bus_park_regions_pie_chart(bus_park_id, from_date, to_date):
    data = []
    if bus_park_id is None:
        dehs = get_almaty_drivers(from_date, to_date, 0)
    else:
        dehs = get_bus_park_drivers(bus_park_id, from_date, to_date, 0)

    for r in Region.objects.all():
        data.append(
            (str(r), dehs.filter(driver__registration_region=r).count()))
    return render_chart(_('Регионы'), None, None, data,
                        _type='pie')
